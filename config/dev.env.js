var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV:		'"development"',
  API_URL:		'"http://localhost:8081/cigen.dev/"',
  UPLOAD_URL:	'"http://localhost:8081/cigen.dev/uploads/"',
  // API_URL: '"http://live.driesen.org/"'

})
