import Vue from "vue"
import Vuex from "vuex"
Vue.use(Vuex)

const defLanguage = "en"
const defPermission = "public"


const defDtaSelected = {}
const defDtaMode = 'view'


const defMainComponent = 'dsnPageHome'

const defEntity = 'cities'
const defEntityEdit = ''
const defEntitySelector = ''

// End Wrapper Settings

export default new Vuex.Store({
    strict: true,
    state: {
        // In fact... the default values...
        language: defLanguage,
        permission: defPermission,
        
        dtaSelected: defDtaSelected,
        dtaMode: defDtaMode,

        mainComponent: defMainComponent,

        // Take care about the Entities...
        entity: defEntity,
        entityEdit: defEntityEdit,
        entitySelector: defEntitySelector,

        formCancelClicked: false,

		products: [
            { name: 'Banana Skin', price: 20},
            { name: 'Product 2', price: 40},
            { name: 'Product 3', price: 60},
            { name: 'Product 4', price: 80},
        ] // End of Products Array

    }, // End of State

    getters: {
        saleProducts: state => {
            var saleProducts =  state.products.map( product => {
                return {
                    name: '**' + product.name + '**',
                    price: product.price,
                } // end of Return
    
            }) // End of Map
            return saleProducts
        }, // End of saleProducts

        mainComponent           : state => { return state.mainComponent },
        
        // Getting the Entity Components...
        entity                  : state => { return state.entity },
        entityEdit              : state => { return state.entityEdit },
        entitySelector          : state => { return state.entitySelector },    

        theLanguage             : state => { return state.language },
        
        thePermission           : state => { return state.permission },
        
        dtaSelected             : state => { return state.dtaSelected },
        dtaMode                 : state => { return state.dtaMode },

        formCancelClicked       : state => { return state.formCancelClicked },

		

    }, // End of getters

    mutations: {
        reducePrice: (state, payload) => {
            state.products.forEach(product => {
                product.price -= payload
            }) // end of ForEach
        }, // End of reducePrice

        changeMainComponent(state, payload) { state.mainComponent = payload },
        
        // Mutating the The Data Entity Comônents...
        changeEntity(state, payload) { state.entity = payload },
        changeEntityEdit(state, payload) { state.entityEdit = payload },
        changeEntitySelector(state, payload) { state.entitySelector = payload },

        changeLanguage(state, payload) { state.language = payload },
        
        changePermission(state, payload) { state.permission = payload },
        
        changeDtaSelected(state, payload) 
            { 
                state.dtaSelected = payload
                state.dtaSelected.dsnIsSelected = true  
            },
        
        changeDtaMode(state, payload) { state.dtaMode = payload },

        changeFormCancelClicked(state, payload) { 
            dsn.warn(null, 'FormCancelClicked changed to ', payload)
            state.formCancelClicked = payload 
        },

        
    }, // End of mutations

    actions: {
        changeMainComponent(context, payload) {
            dsn.warn(null, 'MainComponent Changed in Store', payload)
            context.commit('changeMainComponent', payload)
        },


        // Change the Entity 
        changeEntity(context, payload) {
            dsn.warn(null, 'Entity Changed in Store', payload)
            context.commit('changeEntity', payload)
        },

        // Change the EntityEdit Component
        changeEntityEdit(context, payload) {
            dsn.warn(null, 'EntityEdit Changed in Store', payload)
            context.commit('changeEntityEdit', payload)
        },

        // Change the EntitySelector Component
        changeEntitySelector(context, payload) {
            dsn.warn(null, 'EntitySelector Changed in Store', payload)
            context.commit('changeEntitySelector', payload)
        },
        
        reducePrice: (context, payload) => {
            setTimeout(function () {
                context.commit('reducePrice', payload)
            }, 2000)
        },

        changeLanguage(context, payload) {
            dsn.warn(null, 'Language Changed in Store', payload)
            context.commit('changeLanguage', payload)
        },

        changePermission(context, payload) {
            dsn.warn(null, 'Permission Changed in Store', payload)
            context.commit('changePermission', payload)
        },

        changeDtaSelected(context, payload) {
            context.commit('changeDtaSelected', payload)
        },

        changeDtaMode(context, payload) {
            context.commit('changeDtaMode', payload)
        },

        changeFormCancelClicked(context, payload) {
            context.commit('changeFormCancelClicked', payload)
        },

    }, // End of Actions...

})