import { mapActions } from 'vuex'

export default {
    methods: {
        ...mapActions([
        ]), // end of MapActions

        btnToggleMiniDrawer() {
            dsn.warn(this,"btnToggleDrawer Clicked")
            this.miniDrawer = !this.miniDrawer
            dsn.shout('changedMiniDrawer', this.miniDrawer)
        },

        // Hook Listerers...
        onToggleShowSearch(bValue) {
            dsn.warn(this, 'Hook received', bValue)
            this.showSearch = bValue
        },
  
    }, // End of Methods
    
    
    // Hooks

    mounted() {
        dsn.listen('toggledShowSearch', this.onToggleShowSearch)
    }, // End of mounted
  
    beforeDestroy () {
        dsn.kill('toggledShowSearch', this.onToggleShowSearch)
    }, // End of beforeDestroy
}