// const defLanguage = "en"

import Vue          from 'vue'
import { mapGetters } from 'vuex'

// Language stuff...
import MultiLanguage from 'vue-multilanguage'
import language from '../../lang/language' // Watch out for the path !!!
Vue.use( MultiLanguage, language)


class dsnTrans {
    constructor() {
	} // End of Constructor
	
	mySetters(cmpt) {
		let theLanguage = cmpt.$store.state.language
		let thePermission = cmpt.$store.state.permission
		cmpt.$language = theLanguage
		cmpt.$validator.setLocale(theLanguage)
		// cmpt.$access(thePermission)	
	}

	trans(cmpt,s,o) {
		this.mySetters(cmpt)
		return (cmpt.translate(cmpt.$store.state.language, s,o))
	} // End of trans

	
    transField(comp, ent, fldname, transParam, o) {
        return dsn.trans(comp, ent + '.fields.' + fldname + '.' + transParam, o)
    }

} // End of Class

export { dsnTrans }  