const bShowLog = false
const bShowDebug = false
const bShowWarn = true
const bShowInfo = true

class dsnConsole {
    constructor(a = 12) {
        this.defCmptName = "Unknown Cmpt"
        this.showLog = (process.env.NODE_ENV === 'development') ? bShowLog : false
        this.showDebug = (process.env.NODE_ENV === 'development') ? bShowDebug : false
        this.showWarn = (process.env.NODE_ENV === 'development') ? bShowWarn : false
        this.showInfo = (process.env.NODE_ENV === 'development') ? bShowInfo : false

        this.age = a;
    } // End of Constructor

    log(cmpt = null, s, data = null) {
        let cmptName = (cmpt !== null) ? cmpt.$options.name : this.defCmptName
        if (this.showLog) { console.log('In ' + cmptName + ': ' + s, data) }
    } // End of log

    debug(cmpt = null, s, data = null) {
        let cmptName = (cmpt !== null) ? cmpt.$options.name : this.defCmptName
        if (this.showDebug) { console.debug('In ' + cmptName + ': ' + s, data) }
    } // End of debug

    warn(cmpt = null, s, data = null) {
        let cmptName = (cmpt !== null) ? cmpt.$options.name : this.defCmptName
        if (this.showWarn) { console.warn('In ' + cmptName + ': ' + s, data) }
    } // End of warn

    info(cmpt = null, s, data = null) {
        let cmptName = (cmpt !== null) ? cmpt.$options.name : this.defCmptName
        if (this.showInfo) { console.info('In ' + cmptName + ': ' + s, data) }
    } // End of info
	
} // End of Class

export { dsnConsole }  