import { dsnFunctions } 	from './functions'

import { dsnConsole } 		from './dsnConsole'
import { dsnDataCrud } 		from './dsnDataCrud'
import { dsnEvents } 		from  './dsnEvents'
import { dsnFactory } 		from  './dsnFactory'
import { dsnForm } 			from  './dsnForm'
import { dsnFormatter } 	from  './dsnFormatter'
import { dsnPermission } 	from  './dsnPermission'
import { dsnTrans } 		from  './dsnTrans'
import { dsnUploads }		from  './dsnUploads'


class dsnHelper extends dsnFunctions.aggregation (
    dsnConsole,
	dsnDataCrud,	
    dsnEvents, 
    dsnFactory, 
    dsnForm, 
    dsnFormatter, 
    dsnPermission, 
    dsnTrans,
	dsnUploads,	
	) {
    // May NOT have a constructor !!!
    // constructor(){}
}
 
 
 // var m = new Boy('Mike');
 // m.tellAge(); // Mike is 12 years old.

export { dsnHelper };  