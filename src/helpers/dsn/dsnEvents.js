import Vue from 'vue'

class dsnEvents {
    constructor() {
        this.eventBus = new Vue({
            name: 'defDsnEvents',
            components: {},
            config: {
                silent: false
            },
            data() { return {} },
        })

    } // End of Constructor

    // Start of some global EVENT Functions available on dsn ...

    // emit
    shout(e, data = null) { 
		console.log('shouting', e)
		this.eventBus.$emit(e, data) 
	}

    // on
    listen(e, callback) { this.eventBus.$on(e, callback) }

    // off
    kill(e, callback) { this.eventBus.$off(e, callback) }

    
} // End of Class

export { dsnEvents }