const defModeUndefined = 200
const defModeAdd = 201
const defModeEdit = 202
const defModeSearch = 203
const defModeView = 204

const defIdNo = -999
const defIdLoc = -1
// const defIdExt = -1

class dsnDataCrud {

    constructor(n) {
        // Define some constants which can be used globally ...

        this._modeUndefined = defModeUndefined    
        this._modeAdd = defModeAdd
        this._modeEdit = defModeEdit
        this._modeSearch = defModeSearch
        this._modeView = defModeView

        this._idNo = defIdNo
        this._idLoc = defIdLoc
        // this._idExt = defIdExt

        this.name = n

    } // End of Constructor

    // ================== S T A R T  of the GETTERS and SETTERS... ==========================
    
    // modeUndefined
    get modeUndefined() { return this._modeUndefined } 
    set modeUndefined(value) { this._modeUndefined = value }

    // modeAdd
    get modeAdd() { return this._modeAdd } 
    set modeAdd(value) { this._modeAdd = value }

    // modeEdit
    get modeEdit() { return this._modeEdit }
    set modeEdit(value) { this._modeEdit = value }

    // modeSearch
    get modeSearch() { return this._modeSearch }
    set modeSearch(value) { this._modeSearch = value }

    // modeView
    get modeView() { return this._modeView }
    set modeView(value) { this._modeView = value }

    // idNo
    get idNo() { return this._idNo }
    set idNo(value) { this._idNo = value }

    // idLoc
    get idLoc() { return this._idLoc }
    set idLoc(value) { this._idLoc = value }

    getModeUndefined()    { return (this._modeUndefined) }
    getModeAdd()    { return (this._modeAdd) }
    getModeEdit()   { return (this._modeEdit) }
    getModeView()   { return (this._modeView) }
    getModeSearch() { return (this._modeSearch) }

    // idExt
    // get idExt() { return this._idExt }
    // set idExt(value) { this._idExt = value }
    
    // ================== E N D     of the GETTERS and SETTERS... ==========================

    isInUndefined(comp = null) { return (comp.dsnMode == this._modeUndefined) }

    isInAdd(comp = null) { return (comp.dsnMode == this._modeAdd) } 

    isInEdit(comp = null) { return (comp.dsnMode == this._modeEdit) } 

    isInSearch(comp = null) { return (comp.dsnMode == this._modeSearch) } 

    isInView(comp = null) { return (comp.dsnMode == this._modeView) } 

    
    // =========== S T A R T  of the PUBLIC FUNCTIONS ========================
    setTitleDtaCompType(comp = null, oParams = {}) {
        
        let dtaCompType = comp.compType

        switch (dtaCompType) {
            
            case dsn.dtaCompTypeWrapper:
                return this.setTitleWrapper(comp, oParams)
                break;

            case dsn.dtaCompTypeSelector:
                return this.setTitleSelector(comp, oParams)
                break;

            case dsn.dtaCompTypeInfo:
                return this.setTitleInfo(comp, oParams)
                break;

            case dsn.dtaCompTypeEdit:
                return this.setTitleEdit(comp, oParams)
                break;    
            
            case dsn.dtaCompTypeRecord:
                return this.setTitleRecord(comp, oParams)
                break;    

            case dsn.dtaCompTypeMore:
                return this.setTitleMore(comp, oParams)
                break;    
                
            default:
                dsn.warn(comp, "********** onbekend Type in setTitleDtaCompType", dtaCompType)

        } // End of Switch

    } // End of setTitleDtaCompType
    
    // -- Start of the Selector Specific Functions

    
	doSelectorFilter(comp = null, params = {}) {
        // Removed the NextTick !!!
		// Can possibly cause errors..
		comp.listBag.entities = []
        comp.filterBag.filter = params.filter
        comp.$nextTick(() => { comp.$refs.infiniteLoading.$emit('$InfiniteLoading:reset') })
    } // end of doSelectorFilter
	
	
	doSelectorFilter_keep(comp = null, params = {}) {
        // This one includes the NextTick !!
		
		// comp.dsnData = {}
        // comp.idLoc = dsn.idLoc
        comp.listBag.entities = []
        comp.filterBag.filter = params.filter
        // comp.dsnMode = dsn.modeSearch
        comp.$nextTick(() => { comp.$refs.infiniteLoading.$emit('$InfiniteLoading:reset') })
    } // end of doSelectorFilter


    doSelectorFilterOld(comp = null, params = {}) {
        comp.dsnData = {}
        comp.idLoc = dsn.idLoc
        comp.listBag.entities = []
        comp.filterBag.filter = params.filter
        comp.dsnMode = dsn.modeSearch
        comp.$nextTick(() => { comp.$refs.infiniteLoading.$emit('$InfiniteLoading:reset') })
    } // end of doSelectorFilter


    selectorParams(comp) {
        let params = {}

        // Set the basic parameters for the API ...
        Object.assign(params, {
            page: comp.listBag.entities.length / comp.listBag.perPageFromServer + 1
        })
        Object.assign(params, {
            per_page: comp.listBag.perPageFromServer
        })
        Object.assign(params, {
            sort: comp.filterBag.sortOn
        })

        if ((comp.idLoc === this._idLoc) || (comp.idLoc === undefined)) {
            // Use the filter which is set on this page...
            Object.assign(params, {
                filter: comp.filterBag.filter
            })
            Object.assign(params, {
                filterFields: comp.filterBag.filterOn
            })

        } else {
            // Filter on the external incoming id...
            Object.assign(params, {
                filter: comp.idLoc
            })
            Object.assign(params, {
                filterFields: [{
                    field: 'id'
                }]
            })
        }

        return params
    } // End of selectorParams


    // Needed in the Selector...
    selectorIsSelected(comp, i) {
        return ((i === comp.listBag.selected) || (i == comp.idLoc))
    } // end of selectorIsSelected

    

    selectorResetAll(comp) {
        comp.idToLoad = dsn.idNo
        comp.dsnData = {} 
        comp.dsnMode = dsn.getModeSearch()
        comp.idLoc = this._idLoc
        comp.selectorSearch = '' // clear the text in text input
        comp.listBag.selected = -1
        comp.listBag.entities = []
        
        comp.$refs.infiniteLoading.$emit('$InfiniteLoading:reset')
        
    } // end of selectorResetAll
    
    // -- End of the Selector Specific Functions

    // =========== E N D of the PUBLIC FUNCTIONS ========================



    // =========== START WRAPPER FUNCTIONS ========================
    setTitleWrapper(comp = null, oParams = {}) {
        
        // let ret = ''
        // dsn.warn(comp, 'Nuu in setSelectorTitle !!!!!!!!!!', comp.dtaEntity)

        let sTransSingle = comp.dtaEntity + '.entity.single'
        let sTransPlural = comp.dtaEntity + '.entity.plural'

        return (dsn.trans(comp, sTransSingle, oParams))
    }
    // =========== END WRAPPER FUNCTIONS ========================


    



    // =========== START SELECTOR FUNCTIONS ========================

    setTitleSelector(comp = null, oParams = {}) {

        let ret = ''
        // dsn.warn(comp, 'Nuu in setSelectorTitle !!!!!!!!!!', comp.dtaEntity)

        let sTransSingle = comp.dtaEntity + '.entity.single'
        let sTransPlural = comp.dtaEntity + '.entity.plural'

        let single = dsn.trans(comp, sTransSingle, oParams)
        let plural = dsn.trans(comp, sTransPlural, oParams)

        switch (comp.dsnMode) {
            case this._modeAdd:
                ret = dsn.trans(comp, 'app.selector.title.add', { entity: single })
                break;

            case this._modeEdit:
                ret = dsn.trans(comp, 'app.selector.title.edit', { entity: single })
                break;

            case this._modeSearch:
                ret = dsn.trans(comp, 'app.selector.title.search', { entity: plural })
                break;

            case this._modeView:
                ret = dsn.trans(comp, 'app.selector.title.view', { entity: single })
                break;

            default:
                dsn.warn(comp, 'Mode not found in setSelectorTitle', comp.dsnMode)
        }
        return ret

    } // End of setSelectorTitle


    selectorGetDataSuccessHandler(comp, res) {

        if (res.data.total == 0) {
            dsn.warn(comp, 'geen data gevonden 001')
            
            // Will be depreciated soon...
            // comp.$refs.infiniteLoading.$emit('$InfiniteLoading:complete')
            comp.listBag.$state.complete()
            
            let dtaObject = {} // Empty Object, cause we didn't find anything...
            dsn.shout(comp.triggerBag.onSelectorNoResults, dtaObject)

        } else { // Data Found
            
            // Number of values returned ...
            comp.listBag.total = res.data.total

            // dsn.warn(comp, 'Is data an Object ?', _.isObject(res.data.data))
			if (_.isArray(res.data.data)) {
                // The returned data is an ARRAY (of objects) ...
                if (res.data.data.length) {

                    // Filling the list And Select First One...
                    this.selectorFillList(comp, res.data.data)

                } else { // No (more) data found ...
                    // Will be depreciated soon...
                    // comp.$refs.infiniteLoading.$emit('$InfiniteLoading:complete')
                    comp.listBag.$state.complete()
                }

            } else { // The returned data is an OBJECT ...

                if (res.data.data) {
                    
                    // Filling the list And Select First One...
                    this.selectorFillList(comp, res.data.data)

                } else { // No (more) data found ...
                    // Will be depriciated soon...
                    // comp.$refs.infiniteLoading.$emit('$InfiniteLoading:complete')
                    comp.listBag.$state.complete()
                }

            } // End of Check if return Data is an Object or an Array ...  

        } // End of Data Found

    } // End of selectorSuccessHandler

 
    
    selectorFillList(comp = null, data) {
        // Automatically also selects the first one in the list...
        
        // let dtaObject = {}
        comp.noResults = false

        if (_.isArray(data)) {
            // Filling the list in case of Multiple Data Objects (it's an array)
            comp.listBag.entities = comp.listBag.entities.concat(data)
			// Will be depreciated soon ...
            // comp.$refs.infiniteLoading.$emit('$InfiniteLoading:loaded')
			comp.listBag.$state.loaded()
            comp.idLoc = this._idLoc
        } else {
            // Filling the list with a Single Object..
            comp.listBag.entities = []
            comp.listBag.entities[0] = data
            // Will be depriciated soon...
            // comp.$refs.infiniteLoading.$emit('$InfiniteLoading:loaded')
            comp.listBag.$state.loaded()
            comp.idLoc = comp.listBag.entities[0].id
        }

        // Always Select the first one in the list...
        let dtaObject = comp.listBag.entities[0]
		comp.$store.dispatch('changeDtaSelected', dtaObject)	

        comp.listBag.selected = dtaObject.id
        comp.dtaObject = dtaObject
        // dsn.shout(comp.triggerBag.onSelectorSelect, dtaObject)

        if (comp.listBag.entities.length == comp.listBag.total) {
            // Will be depriciated soon ...
            // comp.$refs.infiniteLoading.$emit('$InfiniteLoading:complete')
            comp.listBag.$state.complete()
        }

    } // End of selectorFillList

    selectorEmptyList(comp = null) {
        comp.listBag.entities = []
    }

    selectorLimitList(comp = null, dtaObject) {
        var filteredList = comp.listBag.entities.filter(function(elem){
            if (elem.id == dtaObject.id) {
                elem.dsnIsSelected = true
                return elem
            }    
        })
        comp.listBag.entities = filteredList
        comp.$store.dispatch('changeDtaSelected', comp.listBag.entities[0])
    }

    selectorUpdateList(comp = null, dtaObject) {
        this.selectorEmptyList(comp)
        dtaObject.dsnIsSelected = true
        comp.listBag.entities[0] = dtaObject
    }
    
    
    loadData(comp = null, id = NaN) {
        let api = ''
        let params = {}
  
        // dsn.warn(null, "nu in loaddata with id", id)
        
        if ( (isNaN(id)) && comp.idToLoad != dsn.idNo ) {
          id = comp.idToLoad
        }
  
        if (isNaN(id)) { // We load ALL the records... eventually with Filter Params
          api     = dsn.apiJSGetAll(comp.dtaEntity)
          params  = dsn.selectorParams(comp)
        } else { // We load Just One record... No Filter Params allowed
          api     = dsn.apiJSGetById(comp.dtaEntity, id)
          params  = {} // This has to be an empty Object !!!
        }
  
        // getData returns a PROMISE !!!!
        dsn.getData(api, params)
          .then((res) => dsn.selectorGetDataSuccessHandler(comp, res))
      } // End of loadData



      onInfinite(comp = null, $state = null) {
        // Had to be added for the new release of Infinite Scroller...
        comp.listBag.$state =  ($state !== null) ? $state : null
        if (_.isEmpty(comp.dsnData)) { dsn.loadData(comp, NaN) } 
        else {
            // Stop loading cause we have external incoming data !!!!
            dsn.warn(null, "UUUSSSINNNGGG Existing data")
            
			// Will be depreciated soon
			// comp.$refs.infiniteLoading.$emit('$InfiniteLoading:complete')
			comp.listBag.$state.complete()
            
            // depending on the mode
            switch (comp.dsnMode) {
            
                case dsn.modeView:
                    dsn.warn(null, 'IIIIIII aaaaaaaaaam in VIIIIEW')    
                    // Fill the list with one element...
                    dsn.selectorFillList(comp, comp.dsnData)
                    break;

                case dsn.modeEdit:
                    dsn.warn(null, 'IIIIIII aaaaaaaaaam in EEEDIT')    
                    // Fill the list with one element...
                    dsn.selectorFillList(comp, comp.dsnData)
                    break;    
            
                default:
            } // End of Swith    


          
        }
      } // end of onInfinite

    // =========== END SELECTOR FUNCTIONS ========================


    // =========== START INFO FUNCTIONS ========================
    setTitleInfo(comp = null, oParams = {}) {
        
        // let ret = ''
        // dsn.warn(comp, 'Nuu in setSelectorTitle !!!!!!!!!!', comp.dtaEntity)

        let sTransSingle = comp.dtaEntity + '.entity.single'
        let sTransPlural = comp.dtaEntity + '.entity.plural'

        return (dsn.trans(comp, sTransSingle, oParams))
    } // setTitleInfo


    // =========== END INFO FUNCTIONS ========================


    // =========== START EDIT FUNCTIONS ========================
    setTitleEdit(comp = null, oParams = {}) {
        
        dsn.warn(null, "nu in setTitleEdit")

        let sTransSingle = comp.dtaEntity + '.entity.single'
        let sTransPlural = comp.dtaEntity + '.entity.plural'

        return (dsn.trans(comp, sTransSingle, oParams))
    }
    // =========== END EDIT FUNCTIONS ========================


    // =========== START RECORD FUNCTIONS ========================
    setTitleRecord(comp = null, oParams = {}) {
        
        // let ret = ''
        // dsn.warn(comp, 'Nuu in setSelectorTitle !!!!!!!!!!', comp.dtaEntity)

        let sTransSingle = comp.dtaEntity + '.entity.single'
        let sTransPlural = comp.dtaEntity + '.entity.plural'

        return (dsn.trans(comp, sTransSingle, oParams))
    }
    // =========== END RECORD FUNCTIONS ========================

    // =========== START MORE FUNCTIONS ========================
    setTitleMore(comp = null, oParams = {}) {
        
        // let ret = ''
        // dsn.warn(comp, 'Nuu in setSelectorTitle !!!!!!!!!!', comp.dtaEntity)

        let sTransSingle = comp.dtaEntity + '.entity.single'
        let sTransPlural = comp.dtaEntity + '.entity.plural'

        return (dsn.trans(comp, sTransSingle, oParams))
    }
    // =========== END MORE FUNCTIONS ========================

    // Goto View from other Component
    // Has to be optimised cause it does an extra Load...
    // Just for EON Demo
    viewLoadDataSuccess (comp, routeName, res, mode) {
        // dsn.warn(this, "res", res)
  
        let dsnData = res.data.data
        // let dsnMode = dsn.modeView
        
        comp.$router.push ({
          name: routeName,
          params: { 
            dsnData:dsnData, 
            dsnMode:mode 
          } // End of Params
        }) // End of Router Push
  
    } // End of viewLoadDataSuccess


    sayHi() {
        console.log('I am dsnDataCrud');
    } // End of sayHi

} // End of Class 

export {
    dsnDataCrud
}
