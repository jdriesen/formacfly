class dsnForm {
	constructor() { 
		this._bFormCancelClicked = false
	} // End of Constructor
	

	// Count all Errors on a Form
	iFormCountErrors(cmpt = null) { return cmpt.$validator.errors.count() } 
	
	// Detects if the Form is Touched. Only triggered when field loses Focus...
	bFormIsTouched(cmpt = null) { return Object.keys(cmpt.fields).some(key => cmpt.fields[key].touched) }

	// Checks if the Form is Dirty
	bFormIsDirty(cmpt = null) { return Object.keys(cmpt.fields).some(key => cmpt.fields[key].dirty) }  

	// Cleans all Validator Flags (used after Save)
	// .clean = depriciated !!! 
	// FormClean(cmpt = null) { cmpt.$validator.clean() }
	FormClean(cmpt = null) { cmpt.$validator.reset() }	

	// Show Close Button when form is NOT dirty and had NO Errors
    bFormShowClose(comp = null) { return (! this.bFormIsDirty(comp)) && (this.iFormCountErrors(comp) == 0) }

	// Show Cancel Button when form is Changed, no matter if there are errors or not ...
	bFormShowCancel(comp = null) { return (this.bFormIsDirty(comp)) }
	
	// Discard changes and close .. Only shown when Cancel is pressed.
	bFormShowDiscardClose(comp = null) { return (true) }

	// Show Reset Button as soon as smt changed in the form
    bFormShowReset(comp = null) { return ( this.bFormIsDirty(comp) )  }
	
	// ENABLE (!!) Reset Button as soon as smt changed in the form
	bFormDisableReset(comp = null) {return ( ! (this.bFormIsDirty(comp) ) ) }

	// Show Save Button as soon as smt changed in the form
    bFormShowSave(comp = null) {return ( this.bFormIsDirty(comp) ) }
	
	// Disable the Save Button in case of errors...
	bFormDisableSave(comp = null) { return (this.iFormCountErrors(comp) > 0) }
	
} // End of Class

export { dsnForm }  