
/* ********************************************* */
/* ALL THIS IS MOVED TO THE GENERAL CONFIG FILES */
/*
// Local
// Uses the DEV database...
// const defApiDevelopment = 'http://localhost:8081/cigen.vue/'
// const defApiProduction = 'http://develop.driesen.org/'

// Server
// const api = 'http://www.driesen.org/'

*/
/* ********************************************* */


import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios)


// Never even THINK about changing these values !!!
// They are used on backend side !!!!
const defResetHistIsDirty = '0'
const defSetHistIsDirty   = '1'

const jsGetter = '/jsget/'
const jsSaver = '/jssave/'
const jsSaver2 = '/jssave2/'
const jsConstructor = '/jsconstruct/'
const jsLogin = '/zauth/login/'
const jsGetTest = '/zauth/gettest/'

const defPerPageFromServer = 20


class dsnFactory {
    constructor() {
        this._resetHistIsDirty = defResetHistIsDirty
        this._setHistIsDirty   = defSetHistIsDirty

        this._api = process.env.API_URL
		
		//  Start Not needed anymore ...  
		/*
		switch(process.env.NODE_ENV) {
            case 'development':
                this._api = defApiDevelopment
                break;
            case 'production' :
                this._api = defApiProduction
                break;
            default:
                console.log('Invalid process.env.NODE_ENV in dsnFactory', process.env.NODE_ENV)
        } 
		*/
		//  End Not Needed anymore 	
		
    } // End of Constructor


    // Setting or resetting the Dirty Flag in the Input Components..
    // Never ever dare to change this function...
    histIsDirty(bIsChanged = true) { 
        return ((bIsChanged) ? this._setHistIsDirty : this._resetHistIsDirty) 
    } // End of histIsDirty 


     // This one is used in the tables...
    setForeignKey(comp) {
        // returns an Object with the Foreign Keys...
        return {
            'foreignKey'    : comp.foreignKey,
            // 'foreignValue'  : comp.id,
            'foreignValue'  : comp.dsnData.id,
        } // End of setting the ForeignKeys
    }
      
    apiJSGet(dtaEntity = '', id = NaN) { 
        return (isNaN(id)) ? this.apiJSGetAll(dtaEntity) : this.apiJSGetById(dtaEntity, id)
        // return (id === NaN) ? this.apiJSGetAll(dtaEntity) : this.apiJSGetById(dtaEntity, id) 
    } // End of apiJSGet

    apiJSGetBasic(dtaEntity) {
        // return api + dtaEntity + jsGetter
        return this._api + dtaEntity + jsGetter
    } // End of apiJSGetBasic

    apiJSGetAll(dtaEntity) {
        return this.apiJSGetBasic(dtaEntity)
    } // End of apiJSGetAll

    apiJSGetById(dtaEntity, id) {
        return this.apiJSGetBasic(dtaEntity) + id
    } // End of apiJSGetById

    apiJSSave(dtaEntity) {
        // return api + dtaEntity + jsSaver
        return this._api + dtaEntity + jsSaver;
    } // End of apiJSSave

    apiJSSave2(dtaEntity) {
        // return api + dtaEntity + jsSaver2;
        return this._api + dtaEntity + jsSaver2;
    } // End of apiJSSave2

    apiJSConstructor(dtaEntity) {
        // return api + dtaEntity + jsConstructor;
        return this._api + dtaEntity + jsConstructor;
    } // End of apiJSConstructor

    apiJSLogin() {
        // return api + jsLogin;
        return this._api + jsLogin;
    } // End of apiJSLogin

    apiJSGetTest() {
        // return api + jsGetTest;
        return this._api + jsGetTest;
    } // End of apiJSGetTest

    loadDataById(entity= "", id=0) {
        // Returns a Promise
        let api     = dsn.apiJSGetById(entity, id)
        let params  = {} // This has to be an empty Object !!!
        return dsn.getData(api, params)
    } // End of loadDataById    


    saveData(api = '', params = {}) {
        // Saving the Data is done via an AXIOS Getter !!!!
        // Returns a Promise
        return axios.get(api, { params: params, })
    } // End of saveData


    getData(api = '', params = {}) {
        // Returns a Promise
        return axios.get(api, { params: params, })
    } // End of getData

    

    setDataListBag() {
        return {
            total: 0,
            selected: 0,
            entities: [],
            // dtaObjects: [],
            perPageFromServer: 20,
        }
    } // End of setDataListBag 


    hasData(dtaHolder) {
        return (_.isEmpty(dtaHolder)) ? false : !(Object.keys(dtaHolder).length === 0)
    } // End of hasData


} // End of Class

export { dsnFactory }
