class dsnUploads {
    constructor() {
        // UPLOAD_URL ends with a '/'
		this._uploadUrl = process.env.UPLOAD_URL
    } // End of Constructor


    // Setting or resetting the Dirty Flag in the Input Components..
    // Never ever dare to change this function...
    
	getUploadPath(fName) {
		return this._uploadUrl + fName 
    }
    
    getEntityImagPath(entity = '', id = 0 , imgName = '') { 
        // has to return 'uploads\players\images\00000001\kevin_de_bruyne.jpg'
        
        let charLeading = '0'
        let maxLength = 8
        let fmtNumber = String(charLeading.repeat(maxLength) + id).slice(-maxLength)
        let fName =  entity + '/images/' + fmtNumber + '/' + imgName
        return this.getUploadPath(fName) 
    } 
	
} // End of Class

export { dsnUploads }
