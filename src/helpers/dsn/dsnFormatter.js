const defMomentLocale       = "en"

// import moment from 'moment'
window.moment = require('moment');

// import accounting from 'accounting'
window.accounting = require('accounting');


class dsnFormatter {
    constructor() {
		this.currentMomentLocale = defMomentLocale
    } // End of Constructor
	
	changeMomentLocale(lang) {
		this.currentMomentLocale = lang
		this.shout('momentLocaleChanged', lang)
	}
	
	// formatters
	
	fmtNumber(value, digits = 2) {
		return accounting.formatNumber(value, digits)
	}

	
	fmtDTUnixToDate(DTUnix = null, fmt = 'DD/MM/YYYY') {
		// DTUnix = -58634955000
		// dsn.warn(null, 'incoming value', DTUnix)
		// other example: D MMM YYYY
		return (DTUnix === null) ? '' : moment.unix(DTUnix).format(fmt)
	}
	
	
	fmtDTUnixToData(DTUnix = 1, fmt = '') {
		// set the default format...
		// other example: D MMM YYYY
		fmt = (fmt === '') ? "DD/MM/YYYY" : fmt
		// return (DTUnix === null) ? '' : moment.unix(DTUnix).format(fmt)
		return moment(DTUnix).format(fmt)
	}

	fmtDTUnixToDataTime(DTUnix = null, fmt = '') {
		// set the default format...
		fmt = (fmt === '') ? "DD/MM/YY HH:mm:ss" : fmt
		return (DTUnix === null) ? '' : moment.unix(DTUnix).format(fmt)
	}

	
	fmtModifiedOn(DTUnix) {
		return this.fmtDTUnixToDataTime(DTUnix)
	}
	
	
	fmtStartcaseEntity(entity = "") {
		return _.replace(_.startCase(_.camelCase(entity)), ' ', '');
	}
	
	/*
	
	
_.replace('Hi Fred', 'Fred', 'Barney');

// return "My String"
startCase(camelCase('my_string'))


// to uppercase
var res = str.toUpperCase();

// Find string
string.indexOf(substring) !== -1;


// give char at certain position
var res = str.charAt(0);

// remove char at 3rd position
str = str.slice(0, 3) + str.slice(4);
	*/
	



} // End of Class

export { dsnFormatter }