export default {
    entity:{
        single:"Car",
        plural:"Cars"
    },
    fields:{
        id:{
            label:"Id",
            placeholder:"Id"
        },
        name:{
            label:"Name",
            placeholder:"Name"
        },
        price:{
            label:"Price",
            placeholder:"Price"
        },
        picture:{
            label:"Picture",
            placeholder:"Picture"
        },
        description:{
            label:"Description",
            placeholder:"Description"
        },
        modifiedby:{
            label:"Modifiedby",
            placeholder:"Modifiedby"
        },
        modifiedon:{
            label:"Modifiedon",
            placeholder:"Modifiedon"
        },
        histvalue:{
            label:"Histvalue",
            placeholder:"Histvalue"
        },
        histversion:{
            label:"Histversion",
            placeholder:"Histversion"
        },
        historig:{
            label:"Historig",
            placeholder:"Historig"
        }
    }
} 

