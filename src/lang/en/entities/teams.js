export default {
    entity:{
        single:"Team",
        plural:"Teams"
    },
    fields:{
        id:{
            label:"Id",
            placeholder:"Id"
        },
        city_id:{
            label:"City_id",
            placeholder:"City_id"
        },
        sport_id:{
            label:"Sport_id",
            placeholder:"Sport_id"
        },
        name:{
            label:"Name",
            placeholder:"Name"
        },
        picture:{
            label:"Picture",
            placeholder:"Picture"
        },
        stadion:{
            label:"Stadion",
            placeholder:"Stadion"
        },
        founded:{
            label:"Founded",
            placeholder:"Founded"
        },
        description:{
            label:"Description",
            placeholder:"Description"
        },
        modifiedby:{
            label:"Modifiedby",
            placeholder:"Modifiedby"
        },
        modifiedon:{
            label:"Modifiedon",
            placeholder:"Modifiedon"
        },
        histvalue:{
            label:"Histvalue",
            placeholder:"Histvalue"
        },
        histversion:{
            label:"Histversion",
            placeholder:"Histversion"
        },
        historig:{
            label:"Historig",
            placeholder:"Historig"
        }
    }
} 

