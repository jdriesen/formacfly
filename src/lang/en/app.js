export default {
	
	titles: {
		edit : {
			caption	: 'Editttt {entity}',
		},
	},	
		
	buttons: {
		add : {
			caption	: 'Add',
			class	: 'btn btn-success btn-xs',
			icon	: 'glyphicon glyphicon-plus',
		},
		
		all : {
			caption	: 'All',
			class	: 'btn btn-primary btn-xs',
			icon	: 'glyphicon glyphicon-plus',
		},
		
		cancel : {
			caption	: 'Cancel',
			class	: 'btn btn-warning btn-xs',
			icon	: 'glyphicon glyphicon-refresh',
		},
		
		close : {
			caption	: 'Close',
			class	: 'btn btn-warning btn-xs',
			icon	: 'glyphicon glyphicon-refresh',
		},
		
		check : {
			caption	: 'Check',
			class	: 'btn btn-warning btn-xs',
			icon	: 'glyphicon glyphicon-cog',
		},
		
		edit : {
			caption	: 'Edit',
			class	: 'btn btn-warning btn-xs',
			icon	: 'glyphicon glyphicon-pencil',
		},
		
		editcontinue : {
			caption	: 'Continue Editing',
			class	: 'btn btn-warning btn-xs',
			icon	: 'glyphicon glyphicon-pencil',
		},
		
		
		delete : {
			caption	: 'Delete',
			class	: 'btn btn-danger btn-xs',
			icon	: 'glyphicon glyphicon-trash',
		},
		
		
		details : {
			caption	: 'Details',
			class	: 'btn btn-primary btn-xs',
			icon	: 'glyphicon glyphicon-th-list',
		},
		
		discardclose : {
			caption	: 'Discard &amp; Close',
			class	: 'btn btn-primary btn-xs',
			icon	: 'glyphicon glyphicon-th-list',
		},
		
		limit : {
			caption	: 'Limit',
			class	: 'btn btn-warning btn-xs',
			icon	: 'glyphicon glyphicon-filter',
		},
		
		
		reset : {
			caption	: 'Reset',
			class	: 'btn btn-success btn-xs',
			icon	: 'glyphicon glyphicon-save',
		},
		
		
		save : {
			caption	: 'Save',
			class	: 'btn btn-success btn-xs',
			icon	: 'glyphicon glyphicon-save',
		},
		
		saveclose : {
			caption	: 'Save &amp; Close',
			class	: 'btn btn-success btn-xs',
			icon	: 'glyphicon glyphicon-save',
		},
		
		search : {
			caption	: 'Search',
			class	: 'btn btn-primary btn-xs',
			icon	: 'glyphicon glyphicon-search',
		},
		
		
		select : {
			caption	: 'Select',
			class	: 'btn btn-success btn-xs',
			icon	: 'glyphicon glyphicon-open',
		},

		
		showall : {
			caption	: 'Show All',
			class	: 'btn btn-primary btn-xs',
			icon	: 'glyphicon glyphicon-th-list',
		},
		
		test : {
			caption	: 'Test',
			class	: 'btn btn-warning btn-xs',
			icon	: 'glyphicon glyphicon-th-list',
		},
		
		view : {
			caption	: 'View',
			class	: 'btn btn-primary btn-xs',
			icon	: 'glyphicon glyphicon-zoom-in',
		},
		
		
		// These are the buttons for the Panels...
		
		expandleft : {
			caption	: 'Expand',
			class	: 'btn btn-primary btn-xs',
			icon	: 'glyphicon glyphicon-chevron-left',
		},
		
		expandright : {
			caption	: 'Expand',
			class	: 'btn btn-primary btn-xs',
			icon	: 'glyphicon glyphicon-chevron-right',
		},
		
		collapseright : {
			caption	: 'Sidebar',
			class	: 'btn btn-primary btn-xs',
			icon	: 'glyphicon glyphicon-chevron-left',
		},
	},
	
	
	selector: {
		
		title: {
			add		: 'Add {entity}',
			edit	: 'Editing {entity}',
			search	: 'Search ({entity})',
			view	: 'View ({entity})',
		},
		
		placeholder	: 'Enter search criteria',
		endoflist	: '<strong>End of list</strong>',
		noresults	: '<strong>No results found</strong>',
		btnshowall	: 'Show All',
	},
	

	table: {
		pagination:		"showing record(s) {from} to {to} of {total} record(s)",
		placeholder:	"Search on {filter}",		
		actions:		"Actions",		
	},	
	
	edit: {
		
		title: {
			add:	'Add {entity}',
			edit:	'Edit {entity}',
		},
		
		info: {
			history: {
				caption:	"Last modified by <strong>{user}</strong> on <strong>{date}</strong> at <strong>{time}</strong>",
			},
		},
		
		status: {
			changed: {
				caption:	"Changed",
				class	: 	"btn btn-warning btn-xs",
				icon	: 	"glyphicon glyphicon-save",
				title:		"Data has been changed",
			},
			unchanged: {
				caption:	"No changes",
				class	: 	"btn btn-success btn-xs",
				icon	: 	"glyphicon glyphicon-save",
				title:		"No changes have been made",
			},
			valid: {
				caption:	"Valid",
				class	: 	"btn btn-success btn-xs",
				icon	: 	"glyphicon glyphicon-save",
				title:		"All entered data is valid",
			},
			invalid: {
				caption:	"Invalid",
				class	: 	"btn btn-warning btn-xs",
				icon	: 	"glyphicon glyphicon-save",
				title:		"You've invalid data. Please correct",
			},
			
			history: {
				caption:	"Last modified by <strong>{user}</strong> on <strong>{date}</strong> at <strong>{time}</strong>",
			},
		},
		
		buttons: {
			cancel : {
				caption	: 'Cancel',
				class	: 'btn btn-warning btn-xs',
				icon	: 'glyphicon glyphicon-refresh',
			},
			
			save : {
				caption	: 'Save',
				class	: 'btn btn-primary btn-xs',
				icon	: 'glyphicon glyphicon-save',
			},
			
			saveclose : {
				caption	: 'Save &amp; Close',
				class	: 'btn btn-success btn-xs',
				icon	: 'glyphicon glyphicon-save',
			},
		
		},
		
		
		toaster: {
			save : {
				title:	'Record Saved',
				msg:	'Your data is successfully saved',
			},
			
			outside : {
				title:	'You have unsaved data',
				msg:	'Please finish your data entry action... (press Cancel or Save)',
			},
			
			
			
			cancel : {
				caption: 'Cancel'
			},
			
		},
		
		prevent: {
			message: '<strong>Warning: </strong>You have unsaved data. <br />Clicking outside is not allowed',
		},
	},
	
	
	
	
	menufooter: {
		reserved: 'All Rights Reserved since {year}',
	},
	
	menumain: {
		about: {
			caption: "About us",
		},
		
		admin: {
			caption: "Admin (en)",
		},
		
		dashboard: {
			caption: "Dashboard (en)",
		},
		
		entities: {
			caption: "Entities",
		},
		
		friends: {
			caption: "Friends",
		},
		
		hello: {
			caption: "Hellow",
		},
		
		home: {
			caption: "Welcome",
		},
		
		nyi: {
			caption: "NYI (en)",
		},
		
		
		sports: {
			caption: "Sports (en)",
			cities: {
				caption: "Cities",
			},
			teams: {
				caption: "Teams",
			},
			players: {
				caption: "Players",
			},
			sports: {
				caption: "Sports (en)",
			},
		},
		
		tabs: {
			caption: "Template Feat.",
		},
		
		tryout: {
			caption: "Tryouts",
		},
		
		verticaltabs: {
			caption: "Vert. Tabs (en)",
		},
		
		wrapper: {
			caption: "Wrapper (EN)",
		},
		
	},
	
	menutop: {
		language: {
			label:		'Change Language',
			english:	'English',
			dutch:		'Dutch',
		},
		
		permission: {
			label:	'Change Permission',
			admin:	'Admin',
			user:	'User',
			public:	'Not Logged in',
		},
		
		access: {
			login:		'Login',
			logout:		'Logout',
		},
		
		debug: {
			label:	'Debug',
			gui:	'GUI',
		},
		
		welcome: {
			label:		'Welcome {name}',
		},
		
	},
	
	pages : {
		
		about: {
			title		: 'About us',
			subtitle	: "This is the <strong>About us</strong> page",
			imagetext	: "We love to code",
		},
		
		admin: {
			title:		'ADMIN test (en)',
			subtitle:	'This component will only be shown when logged in as ADMIN',
			comment:	'Currently, this is just some LOREM IPSUM text <br /><br />But, we can easily extend functionality of this component...',
		},
		
		dashboard: {
			title:		'DASHBOARD (en)',
			subtitle:	'Dashboard Component',
			comment:	'Currently, this is just some LOREM IPSUM text <br /><br />But, we can easily extend functionality of this component...',
		},
		
		empty: {
			title		: 'No matching data found for <strong>{entity}</strong>',
			subtitle	: 'Try a new search',
		},
		
		error: {
			title		: '404',
			subtitle	: "Ooops! Page Not Found",
			text		: "The <strong>requested URL</strong> was not found on this server. Make sure that the Web site address displayed in the address bar of your browser is spelled and formatted correctly."	
		},
		
		hello: {
			title: 'This is the <strong>HELLO</strong> page',
		},
		
		home: {
			title: 'This is the <strong>HOMEEEEE</strong> page',
		},
		
		loader: {
			title		: 'Loading <strong>{entity}</strong> data...',
			subtitle	: 'Please be patient...',
		},
		
		login: {
			title		: 'The Login Component',
			subtitle	: 'Functionality has to be optimised...',
			comment		: 'For debug purposes, we made our life a bit more easy... These buttons will be removed in final version',

			buttons: {
				admin	: 'Sim. Admin Login (en)',	
				user	: 'Sim. User Login (en)',	
				
			},
			
			username: {
				label:			'Username',
				placeholder:	'Enter your username',
			},
			
			password: {
				label:			'Password',
				placeholder:	'Enter your password',
			},
		},
		
		noaccess: {
			title		: '404',
			subtitle	: "Ooops! No Acccess",
			text		: "You have insuficient rights to access the requested page",	
		},
		
		nyi: {
			title		: "Ooops, Sorry ...",
			subtitle	: "This functionality <strong>is not yet implemented</strong>",
			comment		: "<strong>Please come back later on... We're working on it...</strong>",
		},
		
		tabs: {
			title:		'This page contains some TAB and Validation examples',
			subtitle:	'Feel free to click on the tabs below...',
			
			captions: {
				users			: 'Users Tab',
				layout			: 'Layout tab (en)',
				three			: 'Third Tab',
				validation		: 'Validation',
				datetimetest	: 'Date Time Test',
				admintab		: 'Admin(en)',
				verticaltabs	: 'Veer. Tab(en)',
				
			},
		},
		
		tryout: {
			title		: "Tryout ...",
			subtitle	: "These are some <strong>TRYOUTS</strong>",
			comment		: "EN Comment via <strong>V-HTML</strong> with param {name}",
		},
		
		
		verticaltabs: {
			title:		'Vert. Tabs (en)',
			subtitle:	'This component will only be shown when logged in as ADMIN',
			comment:	'Currently, this is just some LOREM IPSUM text <br /><br />But, we can easily extend functionality of this component...',
		},
		
		wrapper: {
			title:		'Wrapper (en)',
			subtitle:	'This is the WRAPPER Component',
			comment:	'Currently, this is just some LOREM IPSUM text <br /><br />But, we can easily extend functionality of this component...',
		},
		
	},
	
	
	dummy : {
		
		accordion: {
			title:	'Accordion (EN)',
			subtitle:	'Accordion Example',
			
		},
		
		vuetableusers: {
			title:	'Vue Table Users (EN)',
		},
		
		verticaltabs: {
			title:	'Vertical Tabs (EN)',
			subtitle:	'Vertical Tabs Example',
			
		},
		
		users: {
			title:		'Users Tab Title',
			subtitle:	'This component contains a SAMPLE table with users',
		},
		
		layout: {
			title01:	'Project Colors',
			title02:	'Bootstrap Colors',
		},
		
		three: {
			title:	'Third Tab Title',
			subtitle:	'Just another component with id {id}',
			
		},
		
		admintab: {
			title:	'Admin Tab Title',
			subtitle:	'AdminTab Component with id {id}',
			
		},
		
		validation: {
			title:		'Validation test',
			subtitle:	'Using Vee-Validate',
			
			name: {
				label:			'Name',
				placeholder:	'Enter your name',
			},
			
			address: {
				label:			'Address',
				placeholder:	'Enter your address',
			},
			
			email: {
				label:			'Email',
				placeholder:	'Enter your Email address',
			},
			
			age: {
				label:			'Age',
				placeholder:	'How old are you',
			},
			
			color: {
				label:			'Color',
				placeholder:	'Select a color',
			},
			
			gender: {
				label:			'Gender',
				placeholder:	'Select the gender',
			},
			
			terms: {
				label:			'Terms and conditions',
				placeholder:	'Check me',
			},
			
			description: {
				label:			'Description',
				placeholder:	'Please give a short description',
			},
			
		},
		
		datetimetest: {
			title:		'Date time testing',
			subtitle:	"Let's give it a try",
			
			name: {
				label:			'dt Name',
				placeholder:	'dt Enter your name',
			},
			
			date: {
				label:			'dt Date',
				placeholder:	'dt Enter a date',
			},
			
			description: {
				label:			'dt Description',
				placeholder:	'dt Please give a short description',
			},
			
		},
		
	},
	
}
