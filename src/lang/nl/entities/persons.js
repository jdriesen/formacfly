export default {
    entity:{
        single:"Person",
        plural:"Persons"
    },
    fields:{
        id:{
            label:"Id",
            placeholder:"Id"
        },
        user_id:{
            label:"User_id",
            placeholder:"User_id"
        },
		name:{
            label:"Name",
            placeholder:"Name"
        },
        firstname:{
            label:"Voornaam",
            placeholder:"Voornaam"
        },
        lastname:{
            label:"Achternaam",
            placeholder:"Achternaam"
        },
        gender:{
            label:"Geslacht",
            placeholder:"Geslacht"
        },
        email:{
            label:"Email",
            placeholder:"Email"
        },
        birthday:{
            label:"Verjaardag",
            placeholder:"Verjaardag"
        },
        description:{
            label:"Description",
            placeholder:"Description"
        },
        modifiedby:{
            label:"Modifiedby",
            placeholder:"Modifiedby"
        },
        modifiedon:{
            label:"Modifiedon",
            placeholder:"Modifiedon"
        },
        histvalue:{
            label:"Histvalue",
            placeholder:"Histvalue"
        },
        histversion:{
            label:"Histversion",
            placeholder:"Histversion"
        },
        historig:{
            label:"Historig",
            placeholder:"Historig"
        }
    }
} 

