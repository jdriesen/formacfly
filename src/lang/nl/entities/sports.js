export default {
    entity:{
        single:"Sport",
        plural:"Sports"
    },
    fields:{
        id:{
            label:"Id",
            placeholder:"Id"
        },
        name:{
            label:"Sport Naam",
            placeholder:"Geef de SportNaam"
        },
        picture:{
            label:"Picture",
            placeholder:"Picture"
        },
        description:{
            label:"Omschrijving",
            placeholder:"Description"
        },
        modifiedby:{
            label:"Modifiedby",
            placeholder:"Modifiedby"
        },
        modifiedon:{
            label:"Modifiedon",
            placeholder:"Modifiedon"
        },
        histvalue:{
            label:"Histvalue",
            placeholder:"Histvalue"
        },
        histversion:{
            label:"Histversion",
            placeholder:"Histversion"
        },
        historig:{
            label:"Historig",
            placeholder:"Historig"
        }
    }
} 

