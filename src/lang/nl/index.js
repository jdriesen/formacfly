import app	from './app'

// Add these blocks to src/lang/en/index.js file
// Do NOT forget to copy the index.js file to all other languages !!
// This file is always the same...

// Entities
// ---- Start Generated block ---
import entityCars	from './entities/cars'
import entityCities	from './entities/cities'
import entityPersons	from './entities/persons'
import entityPlayers	from './entities/players'
import entitySponsors	from './entities/sponsors'
import entitySports	from './entities/sports'
import entityTeams	from './entities/teams'
import entityTeams_sponsors	from './entities/teams_sponsors'
import entityUsers	from './entities/users'
import entityRoles	from './entities/roles'
// ---- End Generated block ---

export default {
	
	'app' : app,
	
	// Add this block to the Export Part of the index.js file
	// Entities
	// ---- Start Generated block ---
	
	'cars'	: entityCars,
	'cities'	: entityCities,
	'persons'	: entityPersons,
	'players'	: entityPlayers,
	'sponsors'	: entitySponsors,
	'sports'	: entitySports,
	'teams'	: entityTeams,
	'teams_sponsors'	: entityTeams_sponsors,
	'users'	: entityUsers,
	'roles'	: entityRoles,
	
	// ---- End Generated block ---

}