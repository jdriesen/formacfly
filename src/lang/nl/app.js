export default {
	
	titles: {
		edit : {
			caption	: 'Wijzig {entity}',
		},
	},	
		
	buttons: {
		add : {
			caption	: 'Toevoegen',
			class	: 'btn btn-success btn-xs',
			icon	: 'glyphicon glyphicon-plus',
		},
		
		all : {
			caption	: 'Alle',
			class	: 'btn btn-primary btn-xs',
			icon	: 'glyphicon glyphicon-plus',
		},
		
		cancel : {
			caption	: 'Annuleren',
			class	: 'btn btn-warning btn-xs',
			icon	: 'glyphicon glyphicon-refresh',
		},
		
		close : {
			caption	: 'Sluiten',
			class	: 'btn btn-warning btn-xs',
			icon	: 'glyphicon glyphicon-refresh',
		},
		
		check : {
			caption	: 'Controleren',
			class	: 'btn btn-warning btn-xs',
			icon	: 'glyphicon glyphicon-cog',
		},
		
		edit : {
			caption	: 'Wijzigen',
			class	: 'btn btn-warning btn-xs',
			icon	: 'glyphicon glyphicon-pencil',
		},
		
		editcontinue : {
			caption	: 'Doorgaan met wijzigen',
			class	: 'btn btn-warning btn-xs',
			icon	: 'glyphicon glyphicon-pencil',
		},
		
		
		delete : {
			caption	: 'Verwijderen',
			class	: 'btn btn-danger btn-xs',
			icon	: 'glyphicon glyphicon-trash',
		},
		
		
		details : {
			caption	: 'Details',
			class	: 'btn btn-primary btn-xs',
			icon	: 'glyphicon glyphicon-th-list',
		},
		
		discardclose : {
			caption	: 'Sluiten zonder bewaren',
			class	: 'btn btn-primary btn-xs',
			icon	: 'glyphicon glyphicon-th-list',
		},
		
		limit : {
			caption	: 'Limiteren',
			class	: 'btn btn-warning btn-xs',
			icon	: 'glyphicon glyphicon-filter',
		},
		
		
		reset : {
			caption	: 'Resetten',
			class	: 'btn btn-success btn-xs',
			icon	: 'glyphicon glyphicon-save',
		},
		
		
		save : {
			caption	: 'Bewaren',
			class	: 'btn btn-success btn-xs',
			icon	: 'glyphicon glyphicon-save',
		},
		
		saveclose : {
			caption	: 'Bewaren &amp; Sluiten',
			class	: 'btn btn-success btn-xs',
			icon	: 'glyphicon glyphicon-save',
		},
		
		search : {
			caption	: 'Zoeken',
			class	: 'btn btn-primary btn-xs',
			icon	: 'glyphicon glyphicon-search',
		},
		
		
		select : {
			caption	: 'Selecteren',
			class	: 'btn btn-success btn-xs',
			icon	: 'glyphicon glyphicon-open',
		},

		
		showall : {
			caption	: 'Alles Tonen',
			class	: 'btn btn-primary btn-xs',
			icon	: 'glyphicon glyphicon-th-list',
		},
		
		test : {
			caption	: 'Testen',
			class	: 'btn btn-warning btn-xs',
			icon	: 'glyphicon glyphicon-th-list',
		},
		
		view : {
			caption	: 'Bekijken',
			class	: 'btn btn-primary btn-xs',
			icon	: 'glyphicon glyphicon-zoom-in',
		},
		
		
		// These are the buttons for the Panels...
		
		expandleft : {
			caption	: 'Expand',
			class	: 'btn btn-primary btn-xs',
			icon	: 'glyphicon glyphicon-chevron-left',
		},
		
		expandright : {
			caption	: 'Expand',
			class	: 'btn btn-primary btn-xs',
			icon	: 'glyphicon glyphicon-chevron-right',
		},
		
		collapseright : {
			caption	: 'Sidebar',
			class	: 'btn btn-primary btn-xs',
			icon	: 'glyphicon glyphicon-chevron-left',
		},
	},
	
	
	selector: {
		
		title: {
			add		: 'Nieuw {entity}',
			edit	: 'Wijzig {entity}',
			search	: 'Zoeken ({entity})',
			view	: 'Bekijk ({entity})',
		},
		
		placeholder	: 'Geef zoekterm',
		endoflist	: '<strong>Einde van de lijst</strong>',
		noresults	: '<strong>Geen resultaten gevonden</strong>',
		btnshowall	: 'Toon alle',
	},
	

	table: {
		pagination:		"Record(s) {from} tot {to} van {total} record(s)",
		placeholder:	"Zoeken op {filter}",
		actions:		"Acties",	
	},
	
	
	edit: {
		
		title: {
			add:	'{entity} toevoegen',
			edit:	'{entity} wijzigen',
		},
		
		info: {
			history: {
				caption:	"Laatst gewijzigd door <strong>{user}</strong> op <strong>{date}</strong> om <strong>{time}</strong>",
			},
		},
		
		status: {
			changed: {
				caption:	"Gewijzigd",
				class	: 	"btn btn-warning btn-xs",
				icon	: 	"glyphicon glyphicon-save",
				title:		"De gegevens zijn gewijzigd",
			},
			unchanged: {
				caption:	"Ongewijzigd",
				class	: 	"btn btn-success btn-xs",
				icon	: 	"glyphicon glyphicon-save",
				title:		"De gegevens zijn nog niet gewijzigd",
			},
			valid: {
				caption:	"Geldig",
				class	: 	"btn btn-success btn-xs",
				icon	: 	"glyphicon glyphicon-save",
				title:		"Alle velden bevatten geldige waarden",
			},
			invalid: {
				caption:	"Ongeldig",
				class	: 	"btn btn-warning btn-xs",
				icon	: 	"glyphicon glyphicon-save",
				title:		"Er foutieve data gedetecteerd. Pas deze aan...",
			},
			
			history: {
				caption:	"Laatst gewijzigd door <strong>{user}</strong> op <strong>{date}</strong> om <strong>{time}</strong>",
			},
		},
		
		buttons: {
			cancel : {
				caption	: 'Ajaaannuleren',
				class	: 'btn btn-warning btn-xs',
				icon	: 'glyphicon glyphicon-refresh',
			},
			
			save : {
				caption	: 'Bewaar',
				class	: 'btn btn-primary btn-xs',
				icon	: 'glyphicon glyphicon-save',
			},
			
			saveclose : {
				caption	: 'Bewaar &amp; Sluit',
				class	: 'btn btn-success btn-xs',
				icon	: 'glyphicon glyphicon-save',
			},
			
		},
		
		
		toaster: {
			save : {
				title:	'Record Bewaard',
				msg:	'De gegevens werden goed bewaard',
			},
			
			outside : {
				title:	'U hebt onbewaarde data',
				msg:	'Vervolledig eerst uw data ingave... (klik Annuleer of Bewaren)',
			},
			
			cancel : {
				caption: 'Cancel'
			},
			
		},
		
		prevent: {
			message: '<strong>Opgelet: </strong>U hebt onbewaarde data.<br />Klik outside is niet toegestaan',
		},
	},
	

	
	menufooter: {
		reserved: 'All rechten voorbehouden sinds {year}',
	},
	
	menumain: {
			
		about: {
			caption: "Over ons",
		},
		
		admin: {
			caption: "Admin (nl)",
		},
		
		dashboard: {
			caption: "Dashboard (nl)",
		},
		
		entities: {
			caption: "Entiteiten",
		},
		
		friends: {
			caption: "Vrienden",
		},
		
		hello: {
			caption: "Hallo !",
		},
		
		home: {
			caption: "Welkom",
		},
		
		nyi: {
			caption: "NYI (nl)",
		},

		sports: {
			caption: "Sports (nl)",
			cities: {
				caption: "Steden",
			},
			teams: {
				caption: "Ploegen",
			},
			players: {
				caption: "Spelers",
			},
			sports: {
				caption: "Sporten (nl)",
			},
		},
		
		tabs: {
			caption: "Template Vbn.",
		},
		
		tryout: {
			caption: "Probeersels",
		},
		
		verticaltabs: {
			caption: "Vert. Tabs (nl)",
		},
		
		wrapper: {
			caption: "Wrapper (nl)",
		},
			
		
	},
	
	menutop: {
		language: {
			label:		'Wijzig taal',
			english:	'Engels',
			dutch:		'Nederlands',
		},
		
		permission: {
			label:	'Wijzig rechten',
			admin:	'Admin',
			user:	'Gebruiker',
			public:	'Niet ingelogged',
		},
		
		access: {
			login:		'Inloggen',
			logout:		'Uitloggen',
		},
		
		debug: {
			label:	'Debug',
			gui:	'GUI',
		},
		
		welcome: {
			label:		'Welkom {name}',
		},
		
	},
	
	pages : {
		
		about: {
			title		: 'Over ons',
			subtitle	: "dit is de <strong>Over Ons</strong> pagina",
			imagetext	: "Wij houden van coderen",
		},
		
		admin: {
			title:		'ADMIN test (nl)',
			subtitle:	'Deze component wordt enkel getoond wanneer men ingelogd is als ADMIN',
			comment:	'Momenteel enkel LOREM IPSUM tekst <br /><br />Maar deze component kan uiteraard uitgebreid wroden...',
		},
		
		dashboard: {
			title:		'DASHBOARD (nl)',
			subtitle:	'Dashboard Component',
			comment:	'Momenteel enkel LOREM IPSUM tekst <br /><br />Maar deze component kan uiteraard uitgebreid wroden...',
		},
		
		
		empty: {
			title		: 'Geen zoekresultaten gevonden voor <strong>{entity}</strong>',
			subtitle	: 'Probeer een nieuwe zoekopdracht',
		},
		
		
		error: {
			title		: '404',
			subtitle	: "Oeps... Pagina niet gevonden",
			text		: "The requested URL was not found on this server. Make sure that the Web site address displayed in the address bar of your browser is spelled and formatted correctly."	
		},
		
		hello: {
			title: 'Dit is de <strong>Hallowkes</strong> pagina',
		},
		
		home: {
			title: 'Dit is de <strong>Welkom</strong> pagina',
		},
		
		loader: {
			title		: '<strong>{entity}</strong> data wordt geladen...',
			subtitle	: 'Een momentje aub...',
		},
		
		login: {
			title		: 'De Login Component',
			subtitle	: 'Let op: dient nog geoptimaliseerd te worden',
			comment		: 'Om debug redenen hebben we het ons even gemakkelijk gemaakt... <br />Deze knoppen zullen vewijderd worden in de final release',

			buttons: {
				admin	: 'Sim. Admin Login (nl)',	
				user	: 'Sim. User Login (nl)',	
				
			},
			
			username: {
				label:			'Gebruikerscode',
				placeholder:	'Geef uw gebruikerscode',
			},
			
			password: {
				label:			'Paswoord',
				placeholder:	'Geef uw paswoord in',
			},
			
		},
		
		noaccess: {
			title		: '404',
			subtitle	: "Oeps! Geen toegang",
			text		: "U hebt onvoldoende rechten om de gevraagde pagina te bekijken",	
		},
		
		nyi: {
			title		: "oooohhhh ... Dit is jammer...",
			subtitle	: "Deze functionaliteit <strong>is nog niet beschikbaar</strong>",
			comment		: "<strong>Deze module is momenteel in ontwilkkeling. Probeer later nog eens opnieuw</strong>",
		},
		
		tabs: {
			title:		'Dit is een pagina met TAB en VALIDATIE voorbeelden',
			subtitle:	'Klik maar eens op onderstaande tabs',
			
			captions: {
				users			: 'Gebruikers Tab',
				layout			: 'Layout tab (nl)',
				three			: 'Derde Tab',
				validation		: 'Validatie',
				datetimetest	: 'Datum and tijd test',
				admintab		: 'Admin(nl)',
				verticaltabs	: 'Veer. Tab(nl)',
				
			},
		},
		
		tryout: {
			title		: "probeersels ...",
			subtitle	: "Dit zijn enkele <strong>Probeersels !!!</strong>",
			comment		: "NL Commentaar via <strong>V-HTML</strong> met param {name}",
		},
		
		verticaltabs: {
			title:		'Vert. Tabs (nl)',
			subtitle:	'Deze component wordt enkel getoond wanneer men ingelogd is als ADMIN',
			comment:	'Momenteel enkel LOREM IPSUM tekst <br /><br />Maar deze component kan uiteraard uitgebreid wroden...',
		},
		
		wrapper: {
			title:		'Wrapper (nl)',
			subtitle:	'Dit is de WRAPPER Component',
			comment:	'Currently, this is just some LOREM IPSUM text <br /><br />But, we can easily extend functionality of this component...',
		},
		
	},
	
	
	dummy : {
		
		accordion: {
			title:	'Accordion (NL)',
			subtitle:	'Accordion Voorbeeld',
			
		},
		
		vuetableusers: {
			title:	'Vue Table Users (NL)',
		},
		
		verticaltabs: {
			title:	'Vertical Tabs (NL)',
			subtitle:	'Vertical Tabs Voorbeeld',
			
		},
		
		users: {
			title:		'Gebruikers Tab Titel',
			subtitle:	'Deze component heeft een VOORBEELD tabel met gebruikers',
		},
		
		layout: {
			title01:	'Project Kleuren',
			title02:	'Booststrap Kleuren',
		},
		
		three: {
			title:	'Derde Tab Titel',
			subtitle:	'Een andere component met id = {id}',
		},
		
		admintab: {
			title:	'Admin Tab Titel',
			subtitle:	'AdminTab Component met id {id}',
			
		},
		
		validation: {
			title:		'Een Validatie test',
			subtitle:	'Implementatie van Vee-validate',
			
			name: {
				label:			'Naam',
				placeholder:	'Geef uw naam',
			},
			
			address: {
				label:			'Woonplaats',
				placeholder:	'Waar woon jij ?',
			},

			
			email: {
				label:			'Maileke',
				placeholder:	'Geef uw Email adres',
			},
			
			age: {
				label:			'Leeftijd',
				placeholder:	'Geef uw Leeftijd',
			},
			
			color: {
				label:			'Kleur',
				placeholder:	'Selecteer een kleur',
			},
			
			gender: {
				label:			'Geslacht',
				placeholder:	'Selecteer het geslacht',
			},
			
			terms: {
				label:			'Algemene voorwaarden',
				placeholder:	'Check mij',
			},
			
			description: {
				label:			'Omschrijving',
				placeholder:	'Geef een korte omschrijving',
			},
		},
		
		
		datetimetest: {
			title:		'Een datum Tijd test',
			subtitle:	'Implementatie van Datum en Tijd mogelijkheden',
			
			name: {
				label:			'dt Naam',
				placeholder:	'dt Geef uw naam',
			},
			
			date: {
				label:			'dt Datum',
				placeholder:	'dt Geef een datum',
			},
			
			description: {
				label:			'dt Omschrijving',
				placeholder:	'dt Geef een korte omschrijving',
			},
			
		},
		
	},
	
}
