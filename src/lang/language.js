import en from './en/index'
import nl from './nl/index'


export default {
	en: en,
	nl: nl
}