// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuetify from 'vuetify'
import './stylus/main.styl'

import App from './App'
import router from './router'
import store  from './store'
import mixins from './mixins'

Vue.mixin(mixins) 

Vue.use(Vuetify)
Vue.config.productionTip = false

import lodash		from 'lodash'
import VueLodash	from 'vue-lodash/dist/vue-lodash.min'
Vue.use(VueLodash, lodash)


// Language Stuff
// const defLanguage = "en"
// import MultiLanguage from 'vue-multilanguage'
// import language from '../src/lang/language' // Watch out for the path !!!
// Vue.use( MultiLanguage, language)

// Validation Stuff
import en from 'vee-validate/dist/locale/en';
import nl from 'vee-validate/dist/locale/nl';
import VeeValidate, { Validator } from 'vee-validate';
// Add locale helper.
Validator.addLocale(en)
Validator.addLocale(nl)
// Install the Plugin and set the locale.
Vue.use(VeeValidate, { locale: 'en' })



import InfiniteLoading from 'vue-infinite-loading'
Vue.component('InfiniteLoading' , InfiniteLoading)


// Importing the DSN Helpers and make the Globally available...
import { dsnHelper }          from './helpers/dsn'
window.dsn = new dsnHelper()




// Start APP Components
import componentsApp from "@/components/app"
// End APP Components

// Start DSN Components
import componentsDSN from "@/components/dsn"
// End DSN Components



// Start Pages Components
import componentsPages from "@/components/pages"
// End APP Components

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
