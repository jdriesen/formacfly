import Vue from 'vue'

// *** Start DSN Components


// Start DSN Root 
import dsnRoot  from '@/components/dsn/Root'
Vue.component('dsnRoot', dsnRoot)


// Start DSN Entity Edit
import dsnEntityEditToolbar  from '@/components/dsn/EntityEditToolbar'
Vue.component('dsnEntityEditToolbar', dsnEntityEditToolbar)

import dsnEntityEditActions  from '@/components/dsn/EntityEditActions'
Vue.component('dsnEntityEditActions', dsnEntityEditActions)

// Start DSN Entity Wrapper
import dsnEntityWrapperToolbar  from '@/components/dsn/EntityWrapperToolbar'
Vue.component('dsnEntityWrapperToolbar', dsnEntityWrapperToolbar)



// *** End DSN Components

export default new Vue({ components: { }, });


