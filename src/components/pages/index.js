import Vue from 'vue'

import dsnPageAbout from '@/components/pages/About'
Vue.component('dsnPageAbout', dsnPageAbout)

import dsnPageAccess from '@/components/pages/Access'
Vue.component('dsnPageAccess', dsnPageAccess)


import dsnPageAdmin from '@/components/pages/Admin'
Vue.component('dsnPageAdmin', dsnPageAdmin)


import dsnPageHome from '@/components/pages/Home'
Vue.component('dsnPageHome', dsnPageHome)



// Friends
import dsnPageAndrea from '@/components/pages/Andrea'
Vue.component('dsnPageAndrea', dsnPageAndrea)

import dsnPageDana    from '@/components/pages/Dana'
Vue.component('dsnPageDana', dsnPageDana)


import dsnPageRaluca    from '@/components/pages/Raluca'
Vue.component('dsnPageRaluca', dsnPageRaluca)




import dsnPageEntities    from '@/components/pages/Entities'
Vue.component('dsnPageEntities', dsnPageEntities)

import dsnPageNYI    from '@/components/pages/NYI'
Vue.component('dsnPageNYI', dsnPageNYI)



export default new Vue({ components: { }, });


