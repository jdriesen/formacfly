import Vue from 'vue'

// Start Application MenuItems
import appMenuTop  from '@/components/app/MenuTop'
Vue.component('appMenuTop', appMenuTop)

import appMenuMain  from '@/components/app/MenuMain'
Vue.component('appMenuMain', appMenuMain)

import appMenuDrawer  from '@/components/app/MenuDrawer'
Vue.component('appMenuDrawer', appMenuDrawer)


// End Application  MenuItems

export default new Vue({ components: { }, });


