import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'

// Start Application Components
import pagesAbout 	from '@/components/pages/About'
import pagesAdmin 	from '@/components/pages/Admin'
import pagesHome 	from '@/components/pages/Home'
// import pagesDag 	from '@/components/pages/Dag'

// import dsnEnhanced 	from '@/components/dsn/Enhanced'
// import dsnEnhanced2 	from '@/components/dsn/Enhanced2'
// import dsnWrapper 	from '@/components/dsn/Wrapper'


import entityCities			from '@/components/entities/cities/Wrapper'
import entityPlayers		from '@/components/entities/players/Wrapper'
import entitySports			from '@/components/entities/Sports/Wrapper'
import entitySponsors		from '@/components/entities/Sponsors/Wrapper'
import entityTeams			from '@/components/entities/teams/Wrapper'



// End Application Components


Vue.use(Router)

export default new Router({
  mode: 'history', // removes the uggly '#'	
  routes: [
    
	// Start HELLO Route
    // Start DAG Route
    {
		path: '/',
		name: 'pagesRoot',
		components: {
			default: pagesHome,
		}, // End of Components
		props: { 
			default: true,  
		}, // End of Props
		meta: {
			permission: 'public|user|admin',
			fail: '/noaccess'
      	}, // End of Meta
	}, // END DAG Route
	
	
	// Start CITIES Route
	{
		path: '/cities',
		name: 'entityCities',
		components: {
			default: entityCities,
		}, // End of Components
		props: { 
			default: true,  
		}, // End of Props
		meta: {
			permission: 'public|user|admin',
			fail: '/noaccess'
      	}, // End of Meta
	}, // END CITIES Route
	

	// Start PLAYERS Route
	{
		path: '/players',
		name: 'entityPlayers',
		components: {
			default: entityPlayers,
		}, // End of Components
		props: { 
			default: true,  
		}, // End of Props
		meta: {
			permission: 'public|user|admin',
			fail: '/noaccess'
      	}, // End of Meta
	}, // END PLAYERS Route
	

	// Start SPORTS Route
	{
		path: '/sports',
		name: 'entitySports',
		components: {
			default: entitySports,
		}, // End of Components
		props: { 
			default: true,  
		}, // End of Props
		meta: {
			permission: 'public|user|admin',
			fail: '/noaccess'
      	}, // End of Meta
	}, // END SPORTS Route
	
	// Start SPONSORS Route
	{
		path: '/sponsors',
		name: 'entitySponsors',
		components: {
			default: entitySponsors,
		}, // End of Components
		props: { 
			default: true,  
		}, // End of Props
		meta: {
			permission: 'public|user|admin',
			fail: '/noaccess'
      	}, // End of Meta
	}, // END SPONSORS Route
	
	// Start TEAMS Route
	{
		path: '/teams',
		name: 'entityTeams',
		components: {
			default: entityTeams,
		}, // End of Components
		props: { 
			default: true,  
		}, // End of Props
		meta: {
			permission: 'public|user|admin',
			fail: '/noaccess'
      	}, // End of Meta
    }, // END SPONSORS Route
	
	
	
	// Start HOME Route
    {
		path: '/home',
		name: 'pagesHome',
		components: {
			default: pagesHome,
		}, // End of Components
		props: { 
			default: true,  
		}, // End of Props
		meta: {
			permission: 'public|user|admin',
			fail: '/noaccess'
      	}, // End of Meta
    }, // END HOME Route
	
	// Start ABOUT Route
    {
		path: '/about',
		name: 'pagesAbout',
		components: {
			default: pagesAbout,
		}, // End of Components
		props: { 
			default: true,  
		}, // End of Props
		meta: {
			permission: 'public|user|admin',
			fail: '/noaccess'
    	}, // End of Meta
	}, // End ABOUT Route
	
	
	// Start ADMIN Route
    {
		path: '/admin',
		name: 'pagesAdmin',
		components: {
			default: pagesAdmin,
		}, // End of Components
		props: { 
			default: true,  
		}, // End of Props
		meta: {
			permission: 'admin',
			fail: '/noaccess'
    	}, // End of Meta
	}, // End ABOUT Route
	
	
	// Start Enhanced2 Route
	/*
    {
		path: '/enh2',
		name: 'dsnEnhanced2',
		components: {
			default: dsnEnhanced2,
		}, // End of Components
		props: { 
			default: true,  
		}, // End of Props
		meta: {
			permission: 'public|user|admin',
			fail: '/noaccess'
		}, // End of Meta
		children: [
			{
			  path: '/enh2/:page',
			  props: true,
			  // component: Details
			  component: dsnWrapper
			}
		  ]
    }, // End ENHANCED2 Route
	*/
  ]
})
