import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'

// Start Application Components
import pagesAbout 	from '@/components/pages/About'
import pagesAdmin 	from '@/components/pages/Admin'
import pagesHome 	from '@/components/pages/Home'
import pagesHello 	from '@/components/pages/Hello'
import pagesDag 	from '@/components/pages/Dag'
import pagesWrapper from '@/components/pages/Wrapper'

import dsnEnhanced from '@/components/dsn/Enhanced'
import dsnEnhanced2 from '@/components/dsn/Enhanced2'
import dsnWrapper from  '@/components/dsn/Wrapper'
import ItemsPage from  '@/components/dsn/ItemsPage'
// End Application Components


Vue.use(Router)

export default new Router({
  mode: 'history', // removes the uggly '#'	
  routes: [
    
	// Start HELLO Route
    {
		path: '/',
		name: 'pagesHello',
		components: {
			default: pagesHello,
		}, // End of Components
		props: { 
			default: true,  
		}, // End of Props
		meta: {
			permission: 'public|user|admin',
			fail: '/noaccess'
      	}, // End of Meta
	}, // END HELLO Route
	

	// Start DAG Route
    {
		path: '/dag',
		name: 'pagesDag',
		components: {
			default: pagesDag,
		}, // End of Components
		props: { 
			default: true,  
		}, // End of Props
		meta: {
			permission: 'public|user|admin',
			fail: '/noaccess'
      	}, // End of Meta
    }, // END DAG Route
	
	
	// Start HOME Route
    {
		path: '/home',
		name: 'pagesHome',
		components: {
			default: pagesHome,
		}, // End of Components
		props: { 
			default: true,  
		}, // End of Props
		meta: {
			permission: 'public|user|admin',
			fail: '/noaccess'
      	}, // End of Meta
    }, // END HOME Route
	
	// Start ABOUT Route
    {
		path: '/about',
		name: 'pagesAbout',
		components: {
			default: pagesAbout,
		}, // End of Components
		props: { 
			default: true,  
		}, // End of Props
		meta: {
			permission: 'public|user|admin',
			fail: '/noaccess'
    	}, // End of Meta
	}, // End ABOUT Route
	
	// Start Enhanced Route
    {
		path: '/enh',
		name: 'dsnEnhanced',
		components: {
			default: dsnEnhanced,
		}, // End of Components
		props: { 
			default: true,  
		}, // End of Props
		meta: {
			permission: 'public|user|admin',
			fail: '/noaccess'
    	}, // End of Meta
	}, // End ABOUT Route
	

	
	
	
	// Start ADMIN Route
    {
		path: '/admin',
		name: 'pagesAdmin',
		components: {
			default: pagesAdmin,
		}, // End of Components
		props: { 
			default: true,  
		}, // End of Props
		meta: {
			permission: 'admin',
			fail: '/noaccess'
    	}, // End of Meta
	}, // End ABOUT Route
	
	// Start WRAPPER Route
    {
		path: '/wrapper',
		name: 'dsnWrapper',
		components: {
			default: dsnWrapper,
		}, // End of Components
		props: { 
			default: true,  
		}, // End of Props
		meta: {
			permission: 'public|user|admin',
			fail: '/noaccess'
    	}, // End of Meta
	}, // End WRAPPER Route
	
	
	// Start Enhanced2 Route
    {
		path: '/enh2',
		name: 'dsnEnhanced2',
		components: {
			default: dsnEnhanced2,
		}, // End of Components
		props: { 
			default: true,  
		}, // End of Props
		meta: {
			permission: 'public|user|admin',
			fail: '/noaccess'
		}, // End of Meta
		children: [
			{
			  path: '/enh2/:page',
			  props: true,
			  // component: Details
			  component: dsnWrapper
			}
		  ]



    }, // End ENHANCED2 Route
	
	
	{
		path: '/details/:page',
		component: ItemsPage,
		props: true,
		children: [
		  {
			path: '/details/:page/:id',
			props: true,
			// component: Details
			component: dsnWrapper
		  }
		]
	},
	
	
	
	
  ]
})
